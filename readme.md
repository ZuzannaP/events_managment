# events management app
a minimal event registration platform


### Quick start

Install dependencies to your virtualenv, using requirements.txt

```
pip install -r requirements.txt
```

The database attached to should be used only for development. It is fully functional as is. However if you wish to deploy this application I suggest using e.g. PostgreSQL. Remember: it is not neccessary for quick set up.
If you wish to configure app with PostreSQL, create new database in PostgreSQL and fill out below data accordingly:


```

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': '',
        'HOST': '',
        'PASSWORD': '',
        'USER': '',
    }
}

```

Fill in missing parameteres with your secret key and your database credentials.

#### Installing

While in directory:

```
events_manager/
```

Run migrations:

```
python3 manage.py migrate
```
Run server

```
python3 manage.py runserver
```


## Built With

* Python 3.7.5
* Django 3.0.6
* Django Rest Framework 3.11.0


## Author

[ZuzannaP](https://github.com/ZuzannaP)

