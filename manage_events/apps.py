from django.apps import AppConfig


class ManageEventsConfig(AppConfig):
    name = 'manage_events'
