from django.shortcuts import render, redirect
from django.views import View

from manage_events.forms import RegistrationForm
from manage_events.models import Event, Participant


class EventsView(View):
    def get(self, request):
        events = Event.objects.all()
        ctx = {"events": events}
        return render(request, "events_tmp.html", ctx)


class RegistrationView(View):
    def get(self, request, pk):
        form = RegistrationForm()
        ctx = {"form": form}
        return render(request, "registration_tmp.html", ctx)

    def post(self, request, pk):
        form = RegistrationForm(request.POST, request.FILES)
        event = Event.objects.get(pk=pk)

        if form.is_valid():
            first_name = form.cleaned_data["first_name"]
            last_name = form.cleaned_data["last_name"]
            email_address = form.cleaned_data["email_address"]
            participant = Participant.objects.create(first_name=first_name, last_name=last_name,
                                                     email_address=email_address)
            event.participants.add(participant)
            return redirect(f"/confirmation/{event.pk}/")

        ctx = {"form": form}
        return render(request, "registration_tmp.html", ctx)


class ConfirmationView(View):
    def get(self, request, pk):
        event = Event.objects.get(pk=pk)
        reservation_nr = 2
        ctx = {"event": event, "reservation_nr": reservation_nr}
        return render(request, "confirmation_tmp.html", ctx)
