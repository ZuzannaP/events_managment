from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import User

from .models import Event, Participant
from django import forms


class RegistrationForm(forms.ModelForm):
    class Meta:
        model = Participant
        fields = "__all__"
