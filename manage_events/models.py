from django.contrib.auth.models import User
from django.db import models


class Participant(models.Model):
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    email_address = models.EmailField(max_length=256)


class Event(models.Model):
    thumbnail_path = models.ImageField(upload_to='thumbnails', max_length=256)
    creation_date = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=256)
    date_time_from = models.DateTimeField()
    date_time_to = models.DateTimeField()
    participants = models.ManyToManyField(Participant, related_name="event_registered")

    def __str__(self):
        return self.title




