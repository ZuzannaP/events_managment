from django.urls import path
from django.conf.urls.static import static

from event_manager.settings import MEDIA_URL, MEDIA_ROOT
from .views import EventsView, ConfirmationView, RegistrationView

#skasuj je
urlpatterns = [
    path('', EventsView.as_view(), name="events"),
    path('registration/<int:pk>', RegistrationView.as_view(), name="register"),
    path('confirmation/<int:pk>', ConfirmationView.as_view(), name="confirmation"),

] + static(MEDIA_URL, document_root=MEDIA_ROOT)
