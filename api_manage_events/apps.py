from django.apps import AppConfig


class ApiManageEventsConfig(AppConfig):
    name = 'api_manage_events'
